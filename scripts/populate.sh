#!/bin/bash
read -rp 'Name (John Smith): ' userName
read -rp 'Username (jsmith): ' userID
read -rsp 'Password: ' userPassword
read -rsp 'Password: ' haPassword
echo
read -rp 'SSID: ' SSID
read -rsp 'WIFI Password: ' wifiPassword
echo
read -rp 'Hostname: ' hostname
read -rp 'disk (nvme1n1): ' disk
encryptedPassword=$(mkpasswd -m yescrypt "$userPassword")

for f in *.ks.sample; do
        cp "$f"  "${f%.*}"
        sed -i "s/<USERID>/$userID/g" "${f%.*}"
        sed -i "s/<FIRSTNAME>/$userName/g" "${f%.*}"
        sed -i "s~<USERPASSWORD>~$encryptedPassword~g" "${f%.*}"
        sed -i "s~<HAPASSWORD>~$haPassword~g" "${f%.*}"
        sed -i "s/<DISKPASSWORD>/$userPassword/g" "${f%.*}"
        sed -i "s/<SSID>/$SSID/g" "${f%.*}"
        sed -i "s/<WIFIPASSWORD>/$wifiPassword/g" "${f%.*}"
        sed -i "s/<HOSTNAME>/$hostname/g" "${f%.*}"
        sed -i "s/<NVME>/$disk/g" "${f%.*}"
done
