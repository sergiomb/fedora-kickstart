# Kickstarter config
This is my kickstarter config.

I use it to deploy Fedora to my personal devices.  It has two flavors a standard one ([kde-base.ks](scripts/kde-base.ks.sample)), and another one with customizations for my Asus Zephyrus GA502IV laptop ([zephyrus.ks](scripts/zephyrus.ks.sample)).

You can get Ventoy [here](https://www.ventoy.net/en/index.html)

You can get the required Fedora ISOs [here](https://download.fedoraproject.org/pub/fedora/linux/releases/36/Everything/x86_64/iso) (link is for Fedora 36, adjust if this document is outdated).  **Note** the standard `live` ISO will not work.  You need the `Everything` ISO!

The rest of this document describes how to configure these files to deploy Fedora with a fully automated install.

**Be Advised** in the configuration below, I install a large number of `flatpak`s.  When deploying, it will appear as though the installion is frozen on the **Running Post Installion Script** step.  It is not frozen, it just has to download a lot of `flatpaks`.  **Be patient**.

## Zephyrus notes

In order for this to work, you must **Disable Secure Boot** before proceeding.  I'm aware that it's possible to sign the nVidia drivers, but i haven't yet tested this.  I may in the future.

All credit to the team at https://asus-linux.org/ for the work done there to make this laptop work in linux.

## Changes to .ks.sample file
In order to generate funcational `.ks` files, simply run the `populate.sh` script and follow the prompts.  The script will compile new `.ks` files for you with the values you specifiy.


## USB Layout
Your Ventoy partion should look roughly like this.  Compare your layout with the [ventoy.json](ventoy/ventoy.json) file.  The documentation for the Ventoy Auto Install Plugin [is here](https://www.ventoy.net/en/plugin_autoinstall.html)

```
.
├── ISO
│   ├── Linux
│   │   ├── Fedora
│   │   │   ├── Fedora-Everything-netinst-x86_64-35-1.2.iso
│   │   │   ├── Fedora-Everything-netinst-x86_64-36-1.5.iso
│   │   ├── Kali
│   │   │   └── kali-linux-2021.4a-installer-amd64.iso
│   │   ├── Nobara
│   │   │   └── Nobara-36-KDE-3.iso
│   │   └── Ubuntu
│   │       └── ubuntu-21.10-desktop-amd64.iso
│   └── Windows
│       ├── en-us_windows_10_consumer_editions_version_21h2_updated_jan_2022_x64_dvd_3c841607.iso
│       └── en-us_windows_11_consumer_editions_updated_jan_2022_x64_dvd_39009ccb.iso
├── scripts
│   └── kde-base.ks
│   └── zephyrus.ks
└── ventoy
    └── ventoy.json
```
